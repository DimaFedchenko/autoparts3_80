﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Nop.Web.Framework.Controllers;
using Nop.Web.Models.Catalog;
using Nop.Web.Controllers;
namespace Nop.Plugin.BadPayBad.HelloWorld.Controllers
{
   public class BadPayBadHelloWorldController : BaseController 
    {
       public ActionResult Configure()
       {
           var model = new object();

            return View("~/Plugins/BadPayBad.HelloWorld/Views/BadPayBadHelloWorld/Configure.cshtml", model);

        }

       public ActionResult FrontEndView()
       {
            var model = new object();
           return View("~/Plugins/BadPayBad.HelloWorld/Views/BadPayBadHelloWorld/FrontEndView.cshtml",new CategoryModel());

        }
    }
}
