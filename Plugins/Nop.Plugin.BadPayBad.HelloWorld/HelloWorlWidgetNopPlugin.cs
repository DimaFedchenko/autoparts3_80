﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Routing;
using Nop.Core.Plugins;
using Nop.Services.Cms;
using Nop.Services.Configuration;
using Nop.Web.Framework.Controllers;

namespace Nop.Plugin.BadPayBad.HelloWorld
{
    [AdminAuthorize]
    public class HelloWorlWidgetNopPlugin : BasePlugin, IWidgetPlugin
    {
       
        public HelloWorlWidgetNopPlugin()
        {
           
        }
        
        public IList<string> GetWidgetZones()
        {
            return new List<string>() { "home_page_top_badpaybad_hello"};
        }

        public void GetDisplayWidgetRoute(string widgetZone, out string actionName, out string controllerName,
            out RouteValueDictionary routeValues)
        {
            actionName = "FrontEndView";
            controllerName = "BadPayBadHelloWorld";
            routeValues = new RouteValueDictionary
            {
                {"Namespaces", "Nop.Plugin.BadPayBad.HelloWorld.Controllers"},
                {"area", null},
                {"widgetZone", widgetZone}
            };
        }

        public void GetConfigurationRoute(out string actionName, out string controllerName,
            out RouteValueDictionary routeValues)
        {
            actionName = "Configure";
            controllerName = "BadPayBadHelloWorld";
            routeValues = new RouteValueDictionary()
            {
                { "Namespaces", "Nop.Plugin.BadPayBad.HelloWorld.Controllers" },
                { "area", null }
            };
        }
    }
}